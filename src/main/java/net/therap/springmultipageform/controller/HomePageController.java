package net.therap.springmultipageform.controller;

import net.therap.springmultipageform.command.ApplicationCommand;
import net.therap.springmultipageform.domain.Application;
import net.therap.springmultipageform.editor.PointsEditor;
import net.therap.springmultipageform.domain.Admin;
import net.therap.springmultipageform.service.AdminService;
import net.therap.springmultipageform.service.ApplicationService;
import net.therap.springmultipageform.validator.ApplicationCommandValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 10/12/21
 */
public class HomePageController {
}
